<?php
 
/**
 * Table tl_layout
 */
$GLOBALS['TL_DCA']['tl_settings']['palettes']['default'] .= ';{recaptcha_legend:hide},siteKey,secrectKey';

$GLOBALS['TL_DCA']['tl_settings']['fields']['siteKey'] = array
(
      'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['siteKey'],
      'inputType'               => 'text',
      'eval'                    => array('decodeEntities'=>true, 'tl_class'=>'w50'),
);

$GLOBALS['TL_DCA']['tl_settings']['fields']['secrectKey'] = array
(
      'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['secrectKey'],
      'inputType'               => 'text',
      'eval'                    => array('decodeEntities'=>true, 'tl_class'=>'w50'),
);
