<?php

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_settings']['siteKey'] = ['reCaptcha site key', 'Please enter your Google site key for reCaptcha'];
$GLOBALS['TL_LANG']['tl_settings']['secrectKey']  = ['reCaptcha secret key', 'Please enter your Google secrect key for reCaptcha'];
$GLOBALS['TL_LANG']['tl_settings']['recaptcha_legend'] = 'ReCaptcha configuration';
