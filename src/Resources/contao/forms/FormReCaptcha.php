<?php
/**
 * Created by PhpStorm.
 * User: cedricbapst
 * Date: 05.09.18
 * Time: 09:18
 */

namespace Formatz\ContaoRecaptcha;

use Contao\CoreBundle\Exception\InternalServerErrorException;


/**
 * Class FormReCaptcha
 * @package FormatZ\ReCaptcha
 */
class FormReCaptcha extends \Widget
{
    const GOOGLE_RECAPTCHA_API = 'https://www.google.com/recaptcha/api.js';
    const GOOGLE_RECAPTCHA_VERIFY = 'https://www.google.com/recaptcha/api/siteverify';

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'recaptcha';

    public function __construct(?array $arrAttributes = null)
    {
        parent::__construct($arrAttributes);

        $this->mandatory = true;
        $this->arrAttributes['required'] = true;

        if (TL_MODE == 'BE') {
            return;
        }

        // Inject Google API
        $GLOBALS['TL_JAVASCRIPT'][] = self::GOOGLE_RECAPTCHA_API;
    }

    /**
     * Validate input
     */
    public function validate()
    {
        $data = array(
            'secret' => $GLOBALS['TL_CONFIG']['secrectKey'],
            'response' => $_POST["g-recaptcha-response"]
        );

        $options = array(
            'http' => array (
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );

        try {
            $context  = stream_context_create($options);

            $verify = file_get_contents(self::GOOGLE_RECAPTCHA_VERIFY, false, $context);
            $captcha_success = json_decode($verify);

            if (is_bool($captcha_success->success) === false) {
                throw new InternalServerErrorException('The ReCaptha success value from Google are not a boolean : ' . $captcha_success->success);
            }

            if ($captcha_success->success === false) {
                $this->addError($GLOBALS['TL_LANG']['ERR']['captcha']);
            }
        } catch (\Exception $e) {
            throw new InternalServerErrorException('ReCaptha error : ' . $e);
        }
    }

    public function generate()
    {
        return sprintf('<div class="g-recaptcha" data-sitekey="%s"></div>',
            $GLOBALS['TL_CONFIG']['siteKey']
        );
    }
}
